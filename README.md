# Hangman

## Features
- Choose random word 
- Let user guess letters 
- Identify where the letter is and display it
- If the letter is wrong add a body part to the hangman
- If the hangman dies the game is over and correct word is displayed
- If the word is guessed the game ends