const wordlist = require("./wordlist.json");
var prompt = require('prompt-sync')();

let health = 0;

function chooseWord() {
    return wordlist[Math.floor(Math.random() * wordlist.length)];
}

function draw(letterArr, word) {
    console.clear();
    let art = ["O","O\n|", " O\n/|", " O\n/|\\", " O\n/|\\\n/", " O\n/|\\\n/ \\"];
    console.log(art[health]);
    console.log(letterArr);
    if(health == 5) {
        console.log("The correct word was: " + word);
    }
}

function guessLetter(letter, word, letterArr) {
    let oldLA = [...letterArr];
    for(let i = 0; i < word.length; i++) {
        if(word[i] == letter) {
            letterArr[i] = letter;
        } 
    }

    let similarLetters = 0;
    let underscores = 0;
    for(let i = 0; i < word.length; i++) {
        if(oldLA[i] == letterArr[i]) {
            similarLetters++;
        }
        if(letterArr[i] == "_") {
            underscores++;
        }
    }
    if(similarLetters == word.length) {
        health++;
    }
    if(underscores == 0) {
        console.clear();
        console.log("You win!\nPress Enter to Continue.");
        return;
    }

    draw(letterArr, word);
}

function inputLetter(word, letterArr) {
    const letter = prompt("");
    guessLetter(letter, word, letterArr);

    if(health < 5) {
        inputLetter(word, letterArr);
    }
}

function main() {
    const word = chooseWord();

    let letterArr = [];
    for(let i = 0; i < word.length; i++) {
        letterArr[i] = "_";
    }

    inputLetter(word, letterArr);
}

main();